#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

############################################################################################
# Etiqueta com POS (tagger de Apertium com dados do ReTratos: Internostrum e Unitex)

# Para etiquetar arquivos em portugues:
# $ perl etiqueta_pos.pl -e in/pt_com_tags.txt -d retratos-apertium-pt -l pt -s out/pt_tagged.txt

# Para etiquetar arquivos em espanhol:
# $ perl etiqueta_pos.pl -e in/es_con_tags.txt -d retratos-apertium-es -l es -s out/es_tagged.txt

# Para etiquetar arquivos em ingles:
# $ perl etiqueta_pos.pl -e in/en_with_tags.txt -d retratos-apertium-en -l en -s out/en_tagged.txt
############################################################################################

use strict;
use locale;
use warnings;
use Getopt::Long;
use Pod::Usage;
use PorTAl::Aux;

my($entrada,$saida,$lang,$dados,$separa,$verbose,$help);

$separa = 1; 		# Versao atual sempre separa tokens

GetOptions( 'entrada|e=s' 			=> \$entrada,
	 			'saida|s=s'				=> \$saida,
				'lingua|l=s'	 		=> \$lang,
				'dados|d=s' 			=> \$dados,
				'verbose|v'				=> \$verbose,
				'help|h'				=> \$help,
			) || pod2usage(2);
	pod2usage(2) if $help;
	pod2usage(2) unless ($entrada && $dados && $saida && $lang);

$dados =~ s/\/$//;

if (($lang ne 'pt') && ($lang ne 'es') && ($lang ne 'en')) {
	print "\n<lingua>: pt (portugues), es (espanhol) ou en (ingles)\n\n";
	exit 1;
}

	if ($verbose) {
		print "\n\tEtiquetando $entrada ... ";
	}

	etiqueta($entrada,$dados,$lang,$saida,$separa);

	if ($verbose) {
		print "OK\n";
	}

exit 0;

sub etiqueta {
	my($entrada,$dados,$lang,$saida,$separa) = @_;
	my($cabf,%sfonte,@idsf,$cont,$original,$comando,$etiquetada,$resultado,$cabs,@tags);

	$cabf = le_sentencas($entrada,\%sfonte,$separa);
	@idsf = sort {compara($a,$b)} keys %sfonte;

	zera_arquivo($saida);

	imprime_cabecalho($saida,$cabf);
	$cont = 1; 	
    @tags = ();

	while ($#idsf >= 0) { # Para cada sentenca original
		$original = $sfonte{$idsf[0]}{'sentenca'};
		encapsula_tags(\$original,\@tags); 	# Protege tags <X> ... </X> para nao serem etiquetadas
		encapsula_chars(\$original);		        # Protege chars especiais para nao serem etiquetados

		# Prepara cabecalho da sentenca que sera impressa
		if (defined($sfonte{$idsf[0]}{'crr'})) {
			$cabs = "<s id=\"".$idsf[0]."\" crr=\"".$sfonte{$idsf[0]}{'crr'}."\" atype=\"".$sfonte{$idsf[0]}{'atype'}."\">";
		}
		else { $cabs = "<s id=\"".$idsf[0]."\">"; }

		if ($verbose) {
			print "IN: $original\n";
		}

		# Prepara comando para etiquetacao
		$comando = 'echo "'.$original.'"'." | lt-proc -a ".$dados."/".$lang.".automorf_retratos.bin | apertium-tagger -gp ".$dados."/".$lang.".prob";
		$etiquetada = `$comando`;	# Executa comando de etiquetacao e guarda sua saida
		$resultado = $?;				# Guarda resultado do comando

		if ($resultado == 0) { # A etiquetacao ocorreu com sucesso, entao imprime saida etiquetada
			$etiquetada =~ s/\^([^ ])/ $1/g;
			$etiquetada =~ s/([^\[])\$/$1 /g;
			$etiquetada =~ s/\n//;
			processa_multipalavras(\$etiquetada);
			desencapsula_tags(\$etiquetada,\@tags);
			desencapsula_chars(\$etiquetada);
		}
		else {
			aviso(1,3,$idsf[0].": ".$original);
			$etiquetada = $original;
			desencapsula_tags(\$etiquetada,\@tags);
			desencapsula_chars(\$etiquetada);
		}
		if ($verbose) {
			print "OUT: $etiquetada\n";
		}

		imprime_sentenca_etiquetada($saida,$cabs,$etiquetada,\$cont);
		shift(@idsf);
	}
	imprime_fim($saida);
}

# Encapsula, entre [ e ], as tags para que nao sejam etiquetadas
sub encapsula_tags {
  my($sent,$array) = @_;
  my($tag,$id);

  if ($$sent =~ /<\/w>/) { # Tag de fim de word eh ignorada
    $$sent =~ s/<\/w>//g;
  }
    
  while ($$sent =~ /<([^>]+)>/) {
    $tag = $1;
    if ($tag =~ /w id=/) { # Texto de entrada ja esta etiquetado com PoS, tag de word
      $id = "tagP".($#$array+1);
    }
    else {
      $id = "tag".($#$array+1);
    }
    $$sent =~ s/<$tag>/[$id]/;
    $tag =~ s/\s*\/\s*/-barra-/g;
    $tag =~ s/ ([\.:-~_]) /$1/g;
    $tag =~ s/^\s+//;
    $tag =~ s/\s+$//;
    push(@$array,$tag);
  }
#	$$sent =~ s/<\s*\/([^>]+)>/[-abref-$1-fecha-]/g;
#	$$sent =~ s/<([^>]+)>/[-abre-$1-fecha-]/g;
}

# Desencapsula tags previamente encapsuladas
sub desencapsula_tags {
  my($sent,$array) = @_;
  my($i,$id);

  for($i=0;$i <= $#$array;$i++) {
    $id = "tag".$i;
    $$sent =~ s/\[$id\]/$$array[$i]\/\*$$array[$i]/g;
    $id = "tagP".$i;
    $$array[$i] =~ s/ /-_-/g;
    $$sent =~ s/\[$id\]\s+/<$$array[$i]>/g;
  }
#  $$sent =~ s/-barra-/\//g;
#	$$sent =~ s/\[-abre-([^>]+)-fecha-\]/<$1>/g;
#	$$sent =~ s/\[-abref-([^>]+)-fecha-\]/<\/$1>/g;
}

sub encapsula {
	my($char) = @_;

	# Oculta chars de aspas pq o Apertium acusa erro se nao encontra fecha de um abre
	if (($char eq "'") || ($char eq "`")) { return "[-<-]"; }
	if (($char eq '"') || ($char eq '”') || ($char eq '“')) { return "[-<<-]"; }

	# Caracteres que o Apertium consegue etiquetar: ! ( ) : ; ? . , 
	# Caracterse que o Apertium nao etiqueta mas tambem nao acusa erro: ' ~ - + = | % & * # _
	if ($char =~ /^[\!\(\):;\?\.,\'\~\-\+\=\|\%\&\*\#\_]$/) { return $char; }

	# O caractere de escape \ precisa ser substituido por outro codigo, no caso, por [---]
	if ($char eq "\\") { return "[---]"; }

	# Caracteres que precisam ser encapsulados entre colchetes ([ e ]) precedidos de \ para serem processados corretamente: " ^ [ ] 
	if ($char =~ /^([\"\^\[\]])$/) { return "[\\$1]"; }

	# Caracteres que precisam ser encapsulados entre colchetes ([ e ]) para serem processados corretamente: @ $ / < >
	if ($char =~ /^([@\$\/<>])$/) { return "[$1]"; }

	# Caracteres que precisam ser encapsulados entre colchetes ([ e ]) mas que permanecerao com os colchetes ao fim do processamento: { }
	if ($char =~ /^([\{\}])$/) { return "[$1]"; }

	# Qualquer outro caractere vai ser encapsulado entre colchetes ([ e ]) precedido de \
	return "[\\$char]";
}

# "Protege" caracteres que o Apertium nao consegue tratar
sub encapsula_chars {
	my($sent) = @_;
	my(@tokens) = split(/ +/,$$sent);

#	map($_ = ($_ !~ /^[\wáéíóúàèìòùãẽĩõũâêîôûäëïöüç]+$/) ? encapsula($_) : $_,@tokens);
	map($_ = ($_ =~ /^\W$/) ? encapsula($_) : $_,@tokens);
	$$sent = join(" ",@tokens);
}

sub desencapsula_chars {
	my($sent) = @_;
	my(@tokens) = split(/ +/,$$sent);

	# Desencapsula char encapsulados previamente
	map($_ = ($_ =~ /^\[\\*(\W)\]$/) ? $1 : $_,@tokens);
	map($_ = ($_ =~ /^\[---\]$/) ? "\\" : $_,@tokens);
	map($_ = ($_ =~ /^\[-<-\]$/) ? "'" : $_,@tokens); # modificado em sem_aspas
	map($_ = ($_ =~ /^\[-<<-\]$/) ? "\"" : $_,@tokens); # modificado em sem_aspas
	map($_ = ($_ =~ /^\[-<<<-\]$/) ? "`" : $_,@tokens); # modificado em sem_aspas
	$$sent = join(" ",@tokens);
}

sub processa_multipalavras {
	my($sent) = @_;
	$$sent =~ s/(\*[^ ]+) /$1> /g;
	$$sent =~ s/([^ ])\# ([^ ]+) /$1\_$2> /g;
	$$sent =~ s/>"/> "/g;
	my @tokens = split(/> /,$$sent);
	map(s/(\w) /$1_/g,@tokens);
	$$sent = join("> ",@tokens);
	$$sent .= ">";
	$$sent =~ s/(\*[^ ]+)>/$1/g;
	$$sent =~ s/(>\_[^>]+)>/$1/g;
}


__END__

=head1 NAME

etiqueta_pos - Etiqueta morfossintaticamente usando dicionarios morfologicos de ReTraTos e Apertium conforme descrito em (CASELI, 2007)

=head1 SYNOPSIS

etiqueta_pos [options...]

 Options:
-entrada|e        arquivo a ser etiquetado (obrigatorio)
-dados|d          diretorio com os dados de ReTraTos-Apertium (obrigatorio)";
-lingua|l         pt, es ou en (obrigatorio)";
-saida|s          arquivo de saida que sera gerado com a saida etiquetada (obrigatorio)"; 
-verbose|v        verbose (opcional)
-help|h|?         esse guia

Exemplo:

  perl etiqueta_pos.pl -e in/pt_com_tags.txt -d retratos-apertim-pt -l pt -s out/pt_tagged.txt

Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)

=cut
